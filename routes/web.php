<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\usersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/user', [usersController::class,'index']);

Route::group(['middleware' => 'roles:1'], function () {
   
Route::get('user/edit/{id}', [usersController::class,'edit']);
Route::GET('user/destroy/{id}', [usersController::class,'destroy']);
});
Route::get('configuracion', [usersController::class,'config']);
Route::GET('user/update/{id}', [usersController::class,'update']);

Route::group(['middleware' => 'roles:1-2'], function () {
   
    Route::get('/user/create', [usersController::class,'create']);
    Route::POST('user/store', [usersController::class,'store']);
  
    });


Route::get('/inicio', function () {
    return view('auth.login');
});
Auth::routes();