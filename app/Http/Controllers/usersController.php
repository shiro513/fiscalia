<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Role;
use App\Models\user_role;
use Auth;
use Request;
use Hash;
class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'users' => User::get(),
        ];
        return view('users.index')->with($data);
      
    }

    public function config()
    {
       
        $user=User::findOrFail(Auth::user()->id); 
        $user_rol=user_role::where('user_id',$user->id)->first();
         
        $data = [
            'rol_user'=>$user_rol->role_id,
            'user' => $user, 
            'roles' => Role::get()->pluck('name','id'),
        ];
        $data['user']->rol;
        
        return view('users.config')->with($data);
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'roles' => Role::get()->pluck('name','id'),
        ];

        return view('users.create')->with($data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function rol($num){
        if ($num == 1) {
            return "Administrador";
        }elseif ($num == 2) {
            return "SuperUsuario";
        }else {
            return "Supervisor";
        }
     }
    public function store(Request $request)
    {
        $inputs = Request::all();
        
        $rules = [
            'name' => 'required|min:c4|max:100',
            'password' => 'required|min:4|max:100',
            'email' => 'required|email|unique:users,email|max:100',

        ];
        $messages = [
            'name.required' => 'El campo nombre es obligatorio',
            'name.min' => 'El nombre debe tener al menos cuatro caracteres',
            'name.max' => 'El nombre debe tener maximo cien caracteres',
            'password.required' => 'El campo contraseña es obligatorio',
            'password.min' => 'El contraseña debe tener al menos cuatro caracteres',
            'password.max' => 'La contraseña debe tener maximo cien caracteres',
            'email.unique' => 'Este correo ya esta en uso',
            'email.email' => 'El correo debe de tener este formato example@email.com',
            'email.required' => 'El campo correo es obligatorio',
            'email.max' => 'El correo debe tener maximo cien caracteres',
        ];
        $validar = Validator::make($inputs, $rules, $messages);
        if ($validar->fails()) {
            
            return Redirect::back()->withInput(Request::all())->withErrors($validar);
        } else {
            
        $inputs['password'] = Hash::make($inputs['password']);
        $name =  $this->rol($inputs['role_id']);
        $role_Administrador = Role::where('name',$name)->first();
        $user = User::create($inputs);
        $user->roles()->attach($role_Administrador);
        session()->flash('success', 'Se ha agregado el usuario correctamente.');
        return Redirect::to('user');   
        }


        // $inputs = $request->validate([
        //     'name' => 'required|min:3|max:50',
        //     'email' => 'required|unique:users|email',
        //     'password' => 'required|min:6',
        // ]);
       
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $user=User::findOrFail($id); 
        $user_rol=user_role::where('user_id',$user->id)->first();
         
        $data = [
            'rol_user'=>$user_rol->role_id,
            'user' => $user, 
            'roles' => Role::get()->pluck('name','id'),
        ];
        $data['user']->rol;
        
        return view('users.edit')->with($data);
       


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = Request::all();
        $user_mail=User::where('id',$id)->where('email',$inputs['email'])->first();
        if ($user_mail) {
            $rules = [
                'name' => 'required|min:4|max:100',
                'email' => 'required|email|max:100',
             
    
            ];
    
            $messages = [
                'name.required' => 'El campo nombre es obligatorio',
                'name.min' => 'El nombre debe tener al menos 4 caracteres',
                'name.max' => 'El nombre debe tener maximo cien caracteres',
                'password.required' => 'El campo contraseña es obligatorio',
                'password.min' => 'El contraseña debe tener al menos 4 caracteres',
                'email.email' => 'El correo debe de tener este formato example@email.com',
                'email.required' => 'El campo correo es obligatorio',
                'email.max' => 'El correo debe tener maximo cien caracteres',
    
              ];
        }else {
            $rules = [
                'name' => 'required|min:4|max:100',
                'email' => 'required|email|unique:users,email|max:100',
             
    
            ];
    
            $messages = [
                'name.required' => 'El campo nombre es obligatorio',
                'name.min' => 'El nombre debe tener al menos 4 caracteres',
                'name.max' => 'El nombre debe tener maximo cien caracteres',
                'password.required' => 'El campo contraseña es obligatorio',
                'password.min' => 'El contraseña debe tener al menos 4 caracteres',
                'email.email' => 'El correo debe de tener este formato example@email.com',
                'email.required' => 'El campo correo es obligatorio',
                'email.max' => 'El correo debe tener maximo cien caracteres',
                'email.unique' => 'Este correo ya esta en uso',
    
              ];
      
        }
        
        $validar = Validator::make($inputs, $rules, $messages);

        if ($validar->fails()) {
            return Redirect::back()->withInput(Request::all())->withErrors($validar);
        } else {
            //logica
            
        $user = User::findOrFail($id);
        $id= $user ->id;
        $user_rol=user_role::where('user_id',$id)->first();
     
        if ($user_rol) {
          $user_rol->role_id=$inputs['role_id'];
          $user_rol->save();

        }
        if ($inputs['password']=='') {
            $datos_del_usuario=[
                'name'=>$inputs['name'],
                'email'=>$inputs['email'],
            ];
            
            $user->fill($datos_del_usuario)->save();
        }
        else{
            $inputs['password']=Hash::make($inputs['password']);
            $datos_del_usuario=[
                'name'=>$inputs['name'],
                'email'=>$inputs['email'],
                'password'=>$inputs['password'],
            ];
            $user->fill($datos_del_usuario)->save();
        }
        
        session()->flash('modifi', 'Se ha mofificado el usuario correctamente.');
        return Redirect::to('user');   
     
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        
        session()->flash('elimine', 'Se ha eliminado el usuario correctamente.');
        return Redirect::to('user');  
    }
}
