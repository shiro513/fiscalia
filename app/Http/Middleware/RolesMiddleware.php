<?php

namespace App\Http\Middleware;

use App\Models\user_role;
use Auth;
use Redirect;
use Closure;
use Illuminate\Http\Request;

class RolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
     
        if(Auth::user()){
            $user_rol=user_role::where('user_id',Auth::user()->id)->first();
         
            $roles = explode("-", $role);
            if(in_array($user_rol->role_id, $roles) == false) {
                return Redirect::to('/user');
            }
        }else{
            return Redirect::to('/login');
        }

        return $next($request);
    }
}
