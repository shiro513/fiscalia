@extends('adminlte::page')

@section('title', 'Usuarios')
@section('plugins.Sweetalert2', true)
@section('content_header')

<link rel="icon" href="{{asset('favicons/favicon.ico')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.6.0/jq-3.6.0/dt-1.11.3/datatables.min.css" />

<h1>Usuarios</h1>
<div class="col-md-2">
	<br>
	@if(Auth::user()->hasRole('Administrador') || Auth::user()->hasRole('SuperUsuario'))
	<a class="btn btn-block bg-gradient-primary" href="{{URL::to('user/create')}}">agregar</a>
	@endif
	<br>


</div>
<div class="box box-primary">
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table id="example" class="table table-condensed table-bordered table-hover">
						<thead>
							<tr>
								<th>nombre</th>
								<th>correo</th>
								<th class="text-center">creado</th>
								<th class="text-center">acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
							@if( $user->id == Auth::user()->id)

							@else
							<tr>
								<td>
									{{ $user->name }}
								</td>
								<td>{{ $user->email }}</td>

								<td class="text-center">{{ $user->created_at->format('d/m/Y H:i') }}</td>
								<td class="text-center">
									@if(Auth::user()->hasRole('Administrador'))
									<a class="btn btn-block bg-gradient-warning" href="{{URL::to('user/edit/'.$user->id)}}">editar</a>

									<a class="btn btn-block bg-gradient-danger" onclic=href="{{URL::to('user/destroy/'.$user->id)}}">eliminar</a>
									@endif

								</td>
							</tr>
							<div class="modal fade" id="modal-delete-{{ $user->id }}">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
											<h4 class="modal-title"><i class="fa fa-warning"></i> Caution!!</h4>
										</div>
										<div class="modal-body">
											<p>Do you really want to delete ({{ $user->name }}) ?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
										</div>
									</div>
								</div>
							</div>
							@endif

							@endforeach
						</tbody>

					</table>
				</div>
			</div>
			<div class="col-md-12 text-center">

			</div>
		</div>
	</div>
</div>

@stop

@section('content')
<p></p>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

<script>
	console.log('Hi!');
</script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.6.0/jq-3.6.0/dt-1.11.3/b-2.0.1/datatables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
<script>
	$('#example').DataTable({
		buttons: [
			'copy', 'excel', 'pdf'
		]
	});
</script>


@if(Session::has('success'))
<script>
	Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'se ha guardado correctamente',
		showConfirmButton: false,
		timer: 1500
	})
</script>
@endif

@if(Session::has('modifi'))
<script>
	Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'se ha modificado correctamente',
		showConfirmButton: false,
		timer: 1500
	})
</script>
@endif
@if(Session::has('elimine'))
<script>
	Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'se ha eliminado correctamente',
		showConfirmButton: false,
		timer: 1500
	})
</script>
@endif
@stop