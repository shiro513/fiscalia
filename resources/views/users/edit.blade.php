@extends('adminlte::page')

@section('title', 'Editar')

@section('content_header')
@stop

@section('content')

<div class="row">
            <div class="col-12">

                @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>
                            Errores:
                        </strong>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif
                </div>
              </div>
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">editar usuario {{$user->name}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {{ Form::open( array('url' => 'user/update/'.$user->id, 'method' => 'GET', 'id' => '', 'class' => '', 'enctype' => 'multipart/form-data' ) ) }}
             

            <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name" placeholder="Nombre">
                    @if($errors->first('name'))
                                    <div class="alert alert-error alert-dismissible fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                  </div>
                  
              
                  <div class="form-group">
                    <label for="exampleInputEmail1">Correo electronico</label>
                    <input type="email" class="form-control" value="{{$user->email}}" name="email" id="email" placeholder="correo">
                    @if($errors->first('email'))
                                    <div class="alert alert-error alert-dismissible fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                  </div>
                 
                  <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" name="password" id="Password" placeholder="Password">
                    @if($errors->first('password'))
                                    <div class="alert alert-error alert-dismissible fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                  </div>
                 
                  <div class="form-group">
                        {{ Form::label ('role_id', 'Rol *', ['class' => '']) }}
                        {!! Form::select('role_id', $roles , $rol_user, ['class' => 'form-control']) !!}
                       
                            <small class="form-text text-danger">{{ $errors->first('role_id') }}</small>
                      
                    </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
                </div>  
             </div>
             {{ Form::close() }}
            <!-- /.card -->

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
