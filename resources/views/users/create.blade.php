@extends('adminlte::page')

@section('title', 'Agregar')

@section('content_header')
@stop

@section('content')
<div class="row">
            <div class="col-12">

                @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>
                            Errores:
                        </strong>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif
</div>
</div>
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">agregar nuevo usuario </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              {{ Form::open( array('url' => 'user/store', 'method' => 'POST', 'id' => '', 'class' => '', 'enctype' => 'multipart/form-data' ) ) }}
              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre Completo</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nombre">
                    @if($errors->first('name'))
                                    <div class="alert alert-error alert-dismissible fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                  </div>
                  
              
                  <div class="form-group">
                    <label for="exampleInputEmail1">Correo Electronico</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
                    @if($errors->first('email'))
                                    <div class="alert alert-error alert-dismissible fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                  </div>
                
                  <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" name="password" id="Password" placeholder="Contraseña">
                    @if($errors->first('password'))
                                    <div class="alert alert-error alert-dismissible fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                  </div>
                  <div id="msg"></div>
 
                  <div class="form-group">
                    <label for="exampleInputPassword1">Confirmar Contraseña</label>
                    <input type="password" class="form-control" name="pasconf" id="pasconf" placeholder="confirma Contraseña">
                  </div>
                  <div class="form-group">
                        {{ Form::label ('role_id', 'Rol *', ['class' => '']) }}
                        {!! Form::select('role_id', $roles , null, ['class' => 'form-control']) !!}
                       
                            <small class="form-text text-danger">{{ $errors->first('role_id') }}</small>
                      
                    </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
                </div>  

                
                {{ Form::close() }}
            </div>
            <!-- /.card -->

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <script>
      .ocultar {
    display: none;
}
 
.mostrar {
    display: block;
}
 
    </script>
@stop

@section('js')

    <script> console.log('Hi!'); </script>
@stop
