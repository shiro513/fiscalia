Hola muy buenas

Espero que se encuentren muy bien, les hago entrega del "Test Dirección de Tecnologías de la Información" 
EN LA PESTAÑA DE LOGIN logearse
un usuario es
Administrador@example.com
contraseña:
123

 Se deberá realizar un CRUD para Altas, Bajas y Modificaciones de Usuarios, el cual tendrá las siguientes restricciones:

1.	El ejercicio se deberá resolver mediante el Framework de Laravel en su Versión 8.
2.	La Integración con la Base de Datos deberá resolverse con SQL Server Versión 2017 o 2019.
3.	El tiempo límite de entrega será el viernes 20 de agosto a las 12:00 PM
4.	El CRUD deberá contener un Login de Acceso con 3 roles diferentes con las siguientes funcionalidades: 
a.	Administrador: Puede crear, editar y eliminar usuarios.
b.	Super-Usuario: Solo puede crear nuevos usuarios.
c.	Supervisor: solo puede listar los usuarios registrados.
5.	La autentificación deberá realizarse con laravel/ui o laravel/breeze.
6.	Se debe crear un filtro para los usuarios autenticados por medio de un middleware que permita acceder a las funcionalidades descritas en el punto número 4.
7.	La base de datos deberá crearse por medio de MIGRACIONES DE LARAVEL con las relaciones y campos señalados en el diagrama 1.
8.	Las inserciones, consultas y/o modificaciones deberán realizarse mediante los modelos en las bases de datos de laravel. 
9.	La Tabla de usuario deberá llenarse con un seeder o factories de 15 usuarios, que permita generar usuarios de prueba aunado de las inserciones generadas por el CRUD.
10.	El formulario de registro y modificación debe de contar un Request de validación para todos los campos de acuerdo al tipo solicitado en la Base de Datos.
en url /user
se muestra el index la tabla de los usuarios no aparece el usuario logueado en la tabla en la parte izquierda se puede editar y si es administrador aparece
un selecc de los roles 
en la parte superior derecha se muestra el nombre y una imagen aleatoria, dando clic en la imagen se abre un cuadro donde abajo del nombre se muestra el rol que tiene
y el log out
atte: Ciro Garcia Torres


